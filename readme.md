# DOC


Text placed in below explains basic idea of restricting query globally, you need those 3 structure to restrict your data,

* the authenticated id 
* read owner id of processing model
* set owner id of processing model
* check processing model has owner

```
        //simply read your authenticated id from global source, it could be auth()->id or what ever it is
        $IdReader = function()
        {
            return app('globalUserId')->getData();
        };

        //read processing model's owner id
        $modelGet = function($model)
        {
            return $model->userId;
        };

        //set processing model's owner id
        $modelSet = function($model,$id)
        {
            $model->userId = $id;
        };

        //check model has owner so we can set or read the current global owner id which comes from $idReader
        $has = function($model)
        {
            return in_array('userId',$model->getFillable());
        };
```

```
        Restriction::define($IdReader,$modelGet,$modelSet,$has); //define restriction by listening queries to inject your id

        //listen globally defined hook to restrict queries by your ownerId (userId)
        Event::listen('queryBuilderConstructionHook',function(\Illuminate\Database\Eloquent\Builder $build) use ($has,$IdReader)
        {
            $build->withGlobalScope('userScope',function(\Illuminate\Database\Eloquent\Builder $builder) use ($has,$IdReader)
            {
                if($has($builder->getModel()))
                {
                    $userId = $IdReader();
                    if($userId)
                        $builder->where('userId',$userId);
                }
            });
        });

```
