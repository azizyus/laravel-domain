<?php


namespace Azizyus\Domain;


/**
 *
 *
 *
 * purpose of class is identifying users by their session
 * since you have this kind of basic structure you can use it for you global restrictions like cart items
 *
 */

class GlobalUserIdentifier
{
    protected $current = null;

    public function get()
    {
        if(!$this->current)
            $this->current = session()->getId();
        return $this->current;
    }

    public function refresh()
    {
        $this->current = null;
        $this->get();
    }

    public function set($data)
    {
        $this->current = $data;
    }

}
