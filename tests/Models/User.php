<?php

namespace Azizyus\Domain\tests\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    protected $fillable = [
        'nameSurname',
        'leIdentifier'
    ];

}
