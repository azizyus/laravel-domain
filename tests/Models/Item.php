<?php

namespace Azizyus\Domain\tests\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{

    protected $fillable = [
        'userId',
        'name'
    ];

}
