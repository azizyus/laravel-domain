<?php

namespace Azizyus\Domain\tests\Unit;

use Azizyus\Domain\DefaultURLParser;
use Azizyus\Domain\DomainRoute;
use Azizyus\Domain\Routes;
use Orchestra\Testbench\TestCase;

class AuthRouteTest extends TestCase
{

    public function defineRoutes($router)
    {
        DomainRoute::baseNoSubDomain()->group(function(){
            Routes::loginRoutes();
            Routes::logoutRoute();
            Routes::routes();
        });
    }

    public function testRoutes()
    {
        DefaultURLParser::parseAndSet('example.com');
        $this->assertEquals('http://example.com/register',route('register'));
        $this->assertEquals('http://example.com/password/forgot',route('passwordResetRequest'));
        $this->assertEquals('http://example.com/password/reset/123',route('passwordResetting','123'));
        $this->assertEquals('http://example.com/password/reset',route('passwordResettingPost'));
        $this->assertEquals('http://example.com/login',route('login'));
        $this->assertEquals('http://example.com/logout',route('logout'));
    }

}
