<?php

namespace Azizyus\Domain\tests\Unit;

use Azizyus\Domain\DomainBuilder;
use Azizyus\Domain\tests\BaseTest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\URL;

class DomainBuilderTest extends BaseTest
{

    public function testIsSameDomain()
    {
        $x = new DomainBuilder();
        $this->assertTrue($x->isSameBaseDomain('google.com','www.google.com'));
        $this->assertTrue($x->isSameBaseDomain('google.com.tr','www.google.com.tr'));

        $this->assertFalse($x->isSameBaseDomain('google.com.tr','www.google.com'));
    }

    public function testWWWCorrection()
    {
        $x = new DomainBuilder();
        $result = $x->wwwCorrection('testt.com','www');
        $this->assertEquals('www.testt.com',$result);
        $result = $x->wwwCorrection('www.testt.com','www');
        $this->assertEquals('www.testt.com',$result);


        $result = $x->wwwCorrection('www.testt.com','base');
        $this->assertEquals('testt.com',$result);
        $result = $x->wwwCorrection('testt.com','base');
        $this->assertEquals('testt.com',$result);


        $result = $x->wwwCorrection('testt.com','any');
        $this->assertEquals('testt.com',$result);
        $result = $x->wwwCorrection('www.testt.com','any');
        $this->assertEquals('www.testt.com',$result);
    }

    public function testRedirection()
    {
        $x = new DomainBuilder();
        $result = $x->expectedRedirection('www.testt.com','www.testt.com');
        $this->assertNull($result);

        $result = $x->expectedRedirection('www.testt.com','testt.com');
        $this->assertNotNull($result);



        /**
         * @var RedirectResponse $response
         */
        $result = $x->expectedRedirection('www.testt.com','testt.com');
        $response = $result('https://www.testt.com/a-kind-of-slug?param=test');
        $this->assertEquals('https://testt.com/a-kind-of-slug?param=test',$response->getTargetUrl());

        /**
         * @var RedirectResponse $response
         */
        $result = $x->expectedRedirection('testt.com','www.testt.com');
        $response = $result('https://testt.com/a-kind-of-slug?param=test');
        $this->assertEquals('https://www.testt.com/a-kind-of-slug?param=test',$response->getTargetUrl());



        /**
         * @var RedirectResponse $response
         */
        $result = $x->expectedRedirection('testt.com','www.testt.com');
        $response = $result('https://www.testt.com/a-kind-of-slug?param=test');
        $this->assertEquals('https://www.testt.com/a-kind-of-slug?param=test',$response->getTargetUrl());
        //this is an edge case of you possible passed current url by your hand it's actually longer than your current one or wrong one


        /**
         * @var RedirectResponse $response
         */
        $result = $x->expectedRedirection('abcd.com','www.testt.com');
        $response = $result('https://testt.com/a-kind-of-slug?param=test');
        $this->assertNull($response);



        /**
         * @var RedirectResponse $response
         */
        $result = $x->expectedRedirection('localhost','localhost.com');
        $response = $result();
        $this->assertEquals('http://localhost.com/php-service',$response->getTargetUrl());
    }

}
