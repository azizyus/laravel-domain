<?php

namespace Azizyus\Domain;


/**
 *
 *
 * this class abstract your "instance" calls by provided user so your db results are where'd by passed user
 * also that user will be passed as param to called func, so you may read possible shop params
 *
 */

class Caller
{

    protected $c;
    protected $instance;
    protected $wrapper;
    public function __construct(callable $c,object $instance,callable $wrapper=null)
    {
        $this->c = $c;
        $this->instance = $instance;
    }

    public function getData()
    {
        $callable = $this->c;
        return $callable();
    }

    public function __call($name, $arguments)
    {
        $c = $this->c;
        $calledCallable = $c();
        return call_user_func_array([$this->instance,$name],array_merge([$calledCallable,$arguments]));
    }

}
