<?php


namespace Azizyus\Domain;


use Illuminate\Support\Facades\Route;

class PrefixedRouteBuilder
{

    public static function build(string $name,$middlewares = [])
    {
        return function($prefix) use ($name,$middlewares){
            return Route::middleware($middlewares)
                ->prefix($name.($prefix !== null ? "/$prefix" : $prefix))
                ->name($name.($prefix!==null ? ".$prefix." : "$prefix."));
        };
    }

}
