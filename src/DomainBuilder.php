<?php

namespace Azizyus\Domain;

use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class DomainBuilder
{

    /**
     * @param $currentDomain
     * @param $baseDomain
     * @return bool
     */
    public function isSameBaseDomain($currentDomain, $baseDomain) : bool
    {
        $x = DefaultURLParser::parse($currentDomain);
        $y = DefaultURLParser::parse($baseDomain);
        return Arr::get($x,'domain') === Arr::get($y,'domain') && Arr::get($x,'tld') === Arr::get($y,'tld');
    }

    /**
     * @param $currentDomain
     * @param $expectedDomain
     * @return \Closure|void
     */
    public function expectedRedirection($currentDomain, $expectedDomain)
    {
        if($currentDomain !== $expectedDomain)
            return function($url = null) use($expectedDomain,$currentDomain) : ?RedirectResponse {
                if(!$url)
                    $url = URL::full();
                $result = Str::contains($url,"$currentDomain");
                if($result) //return redirection if the passed url has the $currentDomain since if it's not a wrong usage of those lines might self-redirect
                    return redirect()->to(str_replace("://$currentDomain","://$expectedDomain",$url));
                return null;
            };
    }

    /**
     * @param $currentHost
     * @param $wwwMode
     * @return mixed|string
     */
    public function wwwCorrection($currentHost, $wwwMode = null)
    {
        $currentDomain = DefaultURLParser::parse($currentHost);
        $mainDomain = $currentHost;

        if($wwwMode === 'www')
        {
            //which means null and we need www as subdomain
            if(!Arr::get($currentDomain,'subdomain'))
            {
                Arr::set($currentDomain,'subdomain','www');
                $mainDomain = implode('.',array_reverse(array_filter($currentDomain)));
            }
        }
        else if($wwwMode === 'base') //set subdomain as null and use base domain for front
        {
             Arr::set($currentDomain,'subdomain',null);
             $mainDomain = implode('.',array_reverse(array_filter($currentDomain)));
        }
        else if($wwwMode === null || $wwwMode === 'any')
        {
            //leaving in purpose, which means we should accept with www. or not
        }
        return $mainDomain;
    }

}
