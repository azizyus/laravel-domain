<?php

namespace Azizyus\Domain\tests\ServiceProvider;

use Azizyus\Domain\Restriction;
use Azizyus\Domain\UserID;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{



    public function boot()
    {

        app()->singleton('globalUserId',function(){
            return new UserID();
        });


        //simply read your authenticated id from global source, it could be auth()->id or what ever it is
        $IdReader = function()
        {
            return app('globalUserId')->getData();
        };

        //read processing model's owner id
        $modelGet = function($model)
        {
            return $model->userId;
        };

        //set processing model's owner id
        $modelSet = function($model,$id)
        {
            $model->userId = $id;
        };

        //check model has owner to we can set or read the current global owner id which comes from $idReader
        $has = function($model)
        {
            return in_array('userId',$model->getFillable());
        };

        Restriction::define($IdReader,$modelGet,$modelSet,$has);


        Event::listen('queryBuilderConstructionHook',function(\Illuminate\Database\Eloquent\Builder $build) use ($has,$IdReader)
        {
            $build->withGlobalScope('userScope',function(\Illuminate\Database\Eloquent\Builder $builder) use ($has,$IdReader)
            {
                if($has($builder->getModel()))
                {
                    $userId = $IdReader();
                    if($userId)
                        $builder->where('userId',$userId);
                }
            });
        });


    }

}
