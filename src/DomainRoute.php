<?php

namespace Azizyus\Domain;

use Illuminate\Support\Facades\Route;

/**
 *
 *
 * base route definition according to your DefaultURLParser
 *
 */

class DomainRoute
{

    public static function base()
    {
        return Route::domain('{subdomain}.{domain}.{tld}');
    }

    public static function baseNoSubDomain()
    {
        return Route::domain('{domain}.{tld}');
    }

    public static function baseSingleWord()
    {
        return Route::domain('{domain}');
    }

}
