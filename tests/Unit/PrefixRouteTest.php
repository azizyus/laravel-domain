<?php

namespace Azizyus\Domain\tests\Unit;

use Azizyus\Domain\DefaultURLParser;
use Azizyus\Domain\DomainRoute;
use Azizyus\Domain\PrefixedRouteBuilder;
use Azizyus\Domain\tests\BaseTest;
use Illuminate\Support\Facades\Route;

class PrefixRouteTest extends BaseTest
{

    protected function defineRoutes($router)
    {
        DomainRoute::baseNoSubDomain()->group(function(){
            PrefixedRouteBuilder::build('user',[])(null)->group(function(){
                Route::get('/')->name('index');
            });
        });

        DomainRoute::baseNoSubDomain()->group(function(){
            PrefixedRouteBuilder::build('user',[])('page')->group(function(){
                Route::get('/')->name('index');
            });
        });

    }

    public function testRouteGeneration()
    {
        DefaultURLParser::parseAndSet('google.com');
        $this->assertEquals('http://google.com/user',route('user.index'));
        $this->assertEquals('http://google.com/user/page',route('user.page.index'));
    }


}
