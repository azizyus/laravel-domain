<?php


namespace Azizyus\Domain;


class UserStoreUrlBuilder
{

    public function build(string $route, string $storeIdentifier, $params=[])
    {
        return route($route,array_merge([
            'subdomain' => $storeIdentifier,
        ],$params));
    }

    public function buildDomain(string $route,string $domain,string $tld,$params=[])
    {
        return route($route,array_merge([
            'domain' => $domain,
            'tld'       => $tld,
        ],$params));
    }


}
