<?php


namespace Azizyus\Domain;


use Illuminate\Database\Eloquent\Builder;

class UniqueStoreIdentifierValidation
{

    public static function val(int $id = null,Builder $builder,string $identifier)
    {
        return function($name,$value,$fail) use($id,$builder,$identifier)
        {
            $result = $builder;
            if($id)
                $result->where('id','!=',$id);
            $result = $result->where($identifier,IdentifierGenerator::generate($value))->exists();
            if($result)
                $fail('store name should be unique');
        };
    }

}
