#mycrypt extension is not provided with the PHP source since 7.2 ,
#but are instead available through PECL. To install a PECL extension in docker,
#use pecl install to download and compile it, then use docker-php-ext-enable to enable it:



FROM php:7.4.13-apache

RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng-dev \
        zlib1g-dev \
        zip \
        libzip-dev \
        libicu-dev \
        openssh-server \
        wget \
    && docker-php-ext-install -j$(nproc) iconv \
#    && pecl install mcrypt-1.0.2 \
#    && docker-php-ext-enable mcrypt  \
    && docker-php-ext-configure gd \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install mysqli pdo_mysql \
    && docker-php-ext-install zip \
    && docker-php-ext-install exif \
    && pecl install xdebug-2.9.0  \
    && docker-php-ext-enable xdebug \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl

ENV APACHE_DOCUMENT_ROOT /var/www/html/serve_
# DOCUMENT ROOT FOR LARAVEL
RUN sed -ri -e "s!/var/www/html!${APACHE_DOCUMENT_ROOT}!g" /etc/apache2/sites-available/*.conf
RUN sed -ri -e "s!/var/www/!${APACHE_DOCUMENT_ROOT}!g" /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf


## SSL

RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/ssl-cert-snakeoil.key -out /etc/ssl/certs/ssl-cert-snakeoil.pem -subj "/C=AT/ST=Vienna/L=Vienna/O=Security/OU=Development/CN=example.com"
RUN a2enmod rewrite
RUN a2ensite default-ssl
RUN a2enmod ssl
## SSL



#### OLD COMPOSER INSTALLER #####
#RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
#RUN php -r "if (hash_file('sha384', 'composer-setup.php') === 'c5b9b6d368201a9db6f74e2611495f369991b72d9c8cbd3ffbc63edff210eb73d46ffbfce88669ad33695ef77dc76976') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
#RUN php composer-setup.php
#RUN php -r "unlink('composer-setup.php');"
#RUN mv composer.phar /composer
#RUN echo "alias composer='/composer'" >> ~/.bashrc
#### OLD COMPOSER INSTALLER #####



## REPO BASED INSTALLER
#RUN wget https://gitlab.com/azizyus/laravel-docker-base-example/-/raw/master/composer-install.sh
#RUN chmod +x ./composer-install.sh && ./composer-install.sh

### OFFICAL COMPOSER INSTALLER
### DOC https://getcomposer.org/doc/faqs/how-to-install-composer-programmatically.md

RUN wget https://raw.githubusercontent.com/composer/getcomposer.org/master/web/installer -O - -q | php -- --quiet
RUN mv composer.phar /composer
RUN echo "alias composer='/composer'" >> ~/.bashrc

RUN wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash
