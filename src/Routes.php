<?php


namespace Azizyus\Domain;


use Illuminate\Support\Facades\Route;

class Routes
{

    public static function loginRoutes()
    {
        Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
        Route::post('login', 'Auth\LoginController@login')->name('login');

    }

    public static function routes()
    {
        static::loginRoutes();
        Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
        Route::post('register', 'Auth\RegisterController@register')->name('register');


        Route::get('password/forgot', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('passwordResetRequest');
        Route::post('password/forgot', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('passwordResetRequest');

        Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('passwordResetting');
        Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('passwordResettingPost');
    }


    public static function logoutRoute()
    {
        Route::any('logout','Auth\LoginController@logout')->name('logout');
    }

}
