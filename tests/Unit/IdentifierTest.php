<?php

namespace Azizyus\Domain\tests\Unit;

use Azizyus\Domain\IdentifierGenerator;
use Azizyus\Domain\tests\BaseTest;
use Azizyus\Domain\tests\Models\User;
use Azizyus\Domain\UniqueStoreIdentifierValidation;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Orchestra\Testbench\TestCase;

class IdentifierTest extends BaseTest
{

    protected function setUp(): void
    {
        parent::setUp();
        Schema::create('users',function (Blueprint $table){
            $table->increments('id');
            $table->string('nameSurname')->nullable();
            $table->string('leIdentifier')->nullable();
            $table->timestamps();
        });
    }

    public function testIdentifierGeneration()
    {
        $x = IdentifierGenerator::generate('tesT name');
        $this->assertEquals('testname',$x);
    }

    public function testIdentifierValidation()
    {
        User::create([
                'leIdentifier' => 'test'
        ]);

        $vFac = function($data){
            return Validator::make(['id' => $data],[
                'id' => UniqueStoreIdentifierValidation::val(null,User::query(),'leIdentifier')
            ]);
        };

        $v = $vFac('test');

        $this->assertTrue($v->fails());

        $v = $vFac('test1');
        $this->assertFalse($v->fails());
    }

    public function testIdentifierIdExcluding()
    {
        $user = User::create([
            'leIdentifier' => 'test'
        ]);

        $vFac = function($data,$id) {
            return Validator::make(['id' => $data],[
                'id' => UniqueStoreIdentifierValidation::val($id,User::query(),'leIdentifier')
            ]);
        };

        $v = $vFac('test',$user->id);
        $this->assertFalse($v->fails());
    }

}
