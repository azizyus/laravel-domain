<?php


namespace Azizyus\Domain;



class GlobalIDWrapper
{

    public static function run(UserID $userID,int $globalUserId,callable $c)
    {
        $oldValue = $userID->getData();
        $userID->setData($globalUserId);
        $result = $c();
        if($oldValue)
            $userID->setData($oldValue);
        else
            $userID->clear();
        return $result;
    }

    //a dummy class which abstracts your calls and injects user as parameter
    public static function wrapperFactory(callable $user,string $class) : Caller
    {
        return new Caller($user,new $class);
    }


}
