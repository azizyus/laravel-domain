<?php

namespace Azizyus\Domain;

use Illuminate\Support\Facades\Event;

/**
 *
 *
 * make sure you set your possible "userId" while saving models so you don't have to deal with it
 * all params as passed as function so all you have to is fill them
 *
 */

class Restriction
{

    private static $registers = [];

    public static function define(callable $IdReader,callable $modelGet,callable $modelSet,callable $has)
    {
        static::$registers[] = function($eventName,array $data) use($IdReader,$modelGet,$modelSet,$has)
        {
            foreach ($data as $model)
            {
                //user id will be injected if your model has "$userColumn"
                $hasUserIdColumn = $has($model);
                if($hasUserIdColumn)
                {
                    if($modelGet($model)) //not empty
                    {
                        //locked id
                        if($modelGet($model) && $modelGet($model) !== $IdReader())
                            throw new LockedUserIdException('you can\'t change key-id of model, it\'s locked');
                    }
                    elseif($IdReader() && !$modelGet($model)) //if it's empty and we have an id to read we can simply set into property
                    {
                        $modelSet($model,$IdReader());
                    }
                }
            }
            return true;
        };

        Event::listen('eloquent.saving: *',function($eventName,array $data)
        {
            foreach (static::$registers as $r)
            {
                $r($eventName,$data);
            }
        });
    }

}
