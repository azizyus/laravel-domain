<?php

namespace Azizyus\Domain\tests\Unit;

use Azizyus\Domain\GlobalIDWrapper;
use Azizyus\Domain\LockedUserIdException;
use Azizyus\Domain\tests\BaseTest;
use Azizyus\Domain\tests\Models\Item;
use Azizyus\Domain\tests\Models\User;
use Azizyus\Domain\tests\ServiceProvider\AppServiceProvider;
use Azizyus\Domain\tests\TestObject\Test;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EQLBuilderTest extends BaseTest
{



    protected function getPackageProviders($app)
    {
        return [AppServiceProvider::class];
    }


    protected function setUp(): void
    {

        parent::setUp();

        Schema::create('users',function (Blueprint $table){
            $table->increments('id');
            $table->string('nameSurname')->nullable();
            $table->string('leIdentifier')->nullable();
            $table->timestamps();
        });

        Schema::create('items',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('userId')->nullable();
            $table->timestamps();
            $table->string('name')->nullable();
        });

    }

    public function testCreation()
    {


        $user = User::create([
            'nameSurname' => 'user_1',
        ]);

        $user2 = User::create([
            'nameSurname' => 'user_2',
        ]);


        $item = Item::create();
        $this->assertNull($item->userId);

        app('globalUserId')->setData($user->id);
        $item = Item::create();
        $this->assertNotNull($item->userId);
        $this->assertNotNull(app('globalUserId')->getData());

    }

    public function testGlobalIdWrapper()
    {

        $user = User::create([
            'nameSurname' => 'user_1',
        ]);

        $user2 = User::create([
            'nameSurname' => 'user_2',
        ]);

        $item = Item::create();
        $this->assertNull($item->userId);
        GlobalIDWrapper::run(app('globalUserId'),$user->id,function()use($user){
            $item = Item::create();
            $this->assertEquals($item->userId,$user->id);
        });
        $this->assertNull($item->userId);
    }

    public function testOwnerIdException()
    {

        $user = User::create([
            'nameSurname' => 'user_1',
        ]);

        $user2 = User::create([
            'nameSurname' => 'user_2',
        ]);

        $item = GlobalIDWrapper::run(app('globalUserId'),$user->id,function()use($user){
            $item = Item::create();
            $this->assertEquals($item->userId,$user->id);
            return $item;
        });

        $this->expectException(LockedUserIdException::class);
        $item->userId = $user2->id; //attempt to change owner
        $item->save();
    }


    public function testOldValue()
    {

        $user = User::create([
            'nameSurname' => 'user_1',
        ]);

        $user2 = User::create([
            'nameSurname' => 'user_2',
        ]);

        app('globalUserId')->setData($user->id);
        $item = Item::create();
        $this->assertEquals($item->userId,$user->id);

        GlobalIDWrapper::run(app('globalUserId'),$user2->id,function()use($user2){
            $item = Item::create();
            $this->assertEquals($item->userId,$user2->id);
            return $item;
        });

    }

    public function testCaller()
    {
        $user = User::create([
            'nameSurname' => 'user_1',
            'leIdentifier' => 'testidentifier',
        ]);

        $c = GlobalIDWrapper::wrapperFactory(function()use($user){
            return $user->leIdentifier;
        },Test::class);
        $this->assertEquals('testidentifier1',$c->addOneToIdentifier());
        $this->assertEquals($user->leIdentifier,$c->getData());
    }

}
