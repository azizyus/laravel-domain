<?php

namespace Azizyus\Domain\tests\Unit;

use Azizyus\Domain\DefaultURLParser;
use Azizyus\Domain\DomainRoute;
use Azizyus\Domain\tests\BaseTest;
use Illuminate\Support\Facades\URL;

class DomainFinderTest extends BaseTest
{



    /**
     * Define routes setup.
     *
     * @param  \Illuminate\Routing\Router  $router
     *
     * @return void
     */
    protected function defineRoutes($router)
    {
        DomainRoute::base()->get('test',function(){
            return 'test123';
        })->name('test');

        DomainRoute::baseNoSubDomain()->get('test2',function(){
            return 'test123';
        })->name('test2');

        DomainRoute::baseSingleWord()->get('test3',function(){
            return 'test123';
        })->name('test3');
    }

    public function testUrlDefault()
    {
        $old = \route('test',['subdomain'=>'test','domain'=>'ff','tld'=>'co.uk']);
        DefaultURLParser::parseAndSet('www.google.com');
        $new = \route('test');

        $this->assertEquals($old,'http://test.ff.co.uk/test');
        $this->assertEquals($new,'http://www.google.com/test');
    }

    public function testUrlDefaultCountryTLD()
    {
        DefaultURLParser::parseAndSet('www.google.co.uk');
        $new = \route('test');
        $this->assertEquals($new,'http://www.google.co.uk/test');
    }

    public function testUrlDefaultCountryTLDWithoutSubdomain()
    {
        DefaultURLParser::parseAndSet('google.co.uk');
        $new = \route('test2');
        $this->assertEquals($new,'http://google.co.uk/test2');
    }

    public function testExampleCom()
    {
        DefaultURLParser::parseAndSet('google.co.uk');
        $new = route('test2');
        $this->assertEquals($new,'http://google.co.uk/test2');
    }

    public function testLocalhost()
    {
        DefaultURLParser::parseAndSet('localhost');
        $new = route('test3');
        $this->assertEquals($new,'http://localhost/test3');
    }

    public function testParseWithScheme()
    {
        $domain = DefaultURLParser::parse('https://www.google.com.tr');
        $this->assertEquals($domain,[
            "tld" => "com.tr",
            "domain" => "google",
            "subdomain" => "www"
        ]);
    }

    public function testParseWithStartTLD()
    {
        $domain = DefaultURLParser::parse('www.comics.com');
        $this->assertEquals($domain,[
            "tld" => "com",
            "domain" => "comics",
            "subdomain" => "www"
        ]);
    }

    public function testParseWithNetTld()
    {
        $domain = DefaultURLParser::parse('www.comics.co.uk');
        $this->assertEquals($domain,[
            "tld" => "co.uk",
            "domain" => "comics",
            "subdomain" => "www"
        ]);
    }

    public function testParseWithBelTld()
    {
        $domain = DefaultURLParser::parse('www.comics.bel.tr');
        $this->assertEquals($domain,[
            "tld" => "bel.tr",
            "domain" => "comics",
            "subdomain" => "www"
        ]);
    }

}
