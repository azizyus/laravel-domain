<?php

namespace Azizyus\Domain;

use Illuminate\Support\Str;

class IdentifierGenerator
{

    public static function generate(string $storeName)
    {
        return Str::slug($storeName,'');
    }

}
