<?php


namespace Azizyus\Domain;


use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\URL;
use Pdp\Domain;
use Pdp\Rules;

/**
 *
 * simply get parse domain and set necessary elements as default to manipulate all routes with low effort
 * it's good for apps that uses xmarket.example.com like structure to identify their marketplaces/data-owner
 *
 *
 */

class DefaultURLParser
{

    /**
     * @var Rules
     */
    private static $rules;


    /**
     * @return Rules
     */
    private static function resolver()
    {
        if(!static::$rules)
            static::$rules = Rules::fromPath(__DIR__.'/../public_suffix_list.dat');
        return static::$rules;
    }

    public static function parse($host,$tld =  null) : array
    {
        $schemePos = strpos($host,'//');
        if($schemePos)
            $host = substr($host,$schemePos+strlen('//'),strlen($host));

        $domain = Domain::fromIDNA2008($host);
        if($domain->count() === 1)
            return [
                'tld' => null,
                'domain' => $domain->toString() ,
                'subdomain' => null,
            ];
        $result = Cache::rememberForever($host,function()use($domain){
            return static::resolver()->resolve($domain);
        });
        return [
            'tld' => $result->suffix()->toString(),
            'domain' => $result->secondLevelDomain()->toString() ,
            'subdomain' => $result->subDomain()->toString()
        ];
    }

    public static function parseAndSet($host)
    {

        //parse, reverse,set and simply match with
        URL::defaults([
            'tld' => null,
            'domain' => null,
            'subdomain' => null,
        ]);

        $parsed  = static::parse($host);
        URL::defaults([
            'tld' => $parsed['tld'],
            'domain' => $parsed['domain'],
            'subdomain' => $parsed['subdomain']
        ]);
    }

}
