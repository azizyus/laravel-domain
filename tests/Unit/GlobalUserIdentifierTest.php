<?php

namespace Azizyus\Domain\tests\Unit;

use Azizyus\Domain\GlobalUserIdentifier;
use Orchestra\Testbench\TestCase;

class GlobalUserIdentifierTest extends TestCase
{

    public function testOperations()
    {
        $instance = new GlobalUserIdentifier();
        $instance->set(1);
        $this->assertEquals(1,$instance->get());
        $instance->refresh();
        $this->assertEquals(session()->getId(),$instance->get());
    }


    public function testInitialData()
    {
        $instance = new GlobalUserIdentifier();
        $this->assertEquals(session()->getId(),$instance->get());
    }

}
