<?php

namespace Azizyus\Domain\tests\Unit;

use Azizyus\Domain\DefaultURLParser;
use Azizyus\Domain\DomainRoute;
use Azizyus\Domain\tests\BaseTest;
use Azizyus\Domain\UserStoreUrlBuilder;

class StoreUrlBuilderTest extends BaseTest
{

    protected function defineRoutes($router)
    {
        DomainRoute::base()->get('test',function(){
            return 'test';
        })->name('test');

        DomainRoute::baseNoSubDomain()->get('test',function(){
            return 'test';
        })->name('test1');




    }

    protected function setUp(): void
    {
        parent::setUp();
        DefaultURLParser::parseAndSet('google.com');
    }

    public function testStoreUrlBuilder()
    {
       $u = new UserStoreUrlBuilder();
       $result = $u->build('test','teststore');
       $this->assertEquals($result,'http://teststore.google.com/test');
    }

    public function testStoreUrlBuilderNoSub()
    {
        $u = new UserStoreUrlBuilder();
        $result = $u->buildDomain('test1','testt','com');
        $this->assertEquals('http://testt.com/test',$result);
    }

}
