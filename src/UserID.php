<?php


namespace Azizyus\Domain;


use Illuminate\Support\Facades\App;

class UserID
{

    protected $userId = null;

    public function setData(int $userId)
    {
        $this->userId = $userId;
    }

    public function clear()
    {
        $this->userId = null;
    }

    public function getData(): ?int
    {
        return $this->userId;
    }

}
